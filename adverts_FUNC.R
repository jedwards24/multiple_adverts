##############
#Notation
#--------
#k - number of arms (ads).
#n - the length of the context vector (number of covariates).
#N - length of time horizon (starts at 1).
#x - the true context (this covariate is 1, all the rest 0).
#xVec - A vector of length N of the values x over the time horizon.
#q - the discrete distribution of x (known but different at each time).
#qMat - an n x N matrix of all the values of q over the time horizon.
#w - the weights of each arm. There are n weights for each arm. These are unknown to the algorithm.
#wMat - a matrix of all weights for all arms (n x k matrix).
#a, b - alpha and beta parameters of the Beta distribution of the belief in each
# arm's weights. There is one a, b parameter for each k arms and n contexts so each will 
# require an n x k matrix to store.
#mu - the mean of the belief distribution given by Beta(a, b).
#runs - number of runs in a simulation.
#PCM/TCM - probablistic and threshold click models.
#clickModel - The true click model used for rewards. The model used by the policies is given
# as the end of the policy name e.g. SGreedyTCM.
#u - random thresholds used in TCM.
#score - an index used by a policy as a proxy for true weights e.g. mean, TS sample from posterior, quantile. 
#FIXq - global boolean controls whether to use same q over time.
#qCONSTANT - global constant gives value of q used if fixed.
#DIRq - global boolean controls whether to use dirichlet distribution for q.
#DIRICHLET_PARAMETERS - parameters used in dirichlet distribution generation.
##############

NRWeight <- 0.0001 #Global parameter for the value assigned to non-relevant weights to avoid any
#arms with all zeroes (for overlap).
if (exists("FIXq")==F){FIXq <- F}
if (exists("DIRq")==F){DIRq <- F}
if (exists("postqType")==F){postqType <- "PCMKnown"}

################################
#Generates a matrix of values for w for a single run using given seed.
#The arguments "aPrior" and "bPrior" are scalars (assumed the same for each arm).
#The values depend on the model used which is given as an argument wModel.
#A function generateWeightsx() is called where x is a number given by wModel.
#See those functions for model explanations.
#Returned matrix has dimension n x k.
#NOTE: it is possible to have an arm with all zero weights.
################################
generateWeightsMaster <- function(k, n, aPrior, bPrior, wSeed, 
                                  wModel=1, relevanceProb=1, bPriorOther=40){
  set.seed(wSeed)
  name <- paste0("generateWeights", wModel)
  return(eval(call(name, k, n, aPrior, bPrior, relevanceProb, bPriorOther)))
}

##############
#Given a weight matrix of non-relevant values this randomly adds relevant value drawn
# from a Beta(aPrior, bPrior)distribution. The number of relevant values in each column
# is given by numRelevantVec.Used by weightModels 2-5 and called in generateWeightsx.
##############
addRelevantWeights <- function(k, n, aPrior, bPrior, relevanceProb, weightMat, numRelevantVec){
  for (i in 1 : k){
    relevant <- sample(n, numRelevantVec[i], replace=F) #values of x for which each arm is relevant
    weightMat[relevant, i] <- rbeta(numRelevantVec[i], aPrior, bPrior)
  }
  return(weightMat)
}

################
#Model 1 - uses a mixture distribution - each is drawn from a Beta(aPrior, bPrior) with 
#probability 'relevanceProb' and zero otherwise.
################
generateWeights1 <- function(k, n, aPrior, bPrior, relevanceProb, bPriorOther){
  relevant <- matrix(rbinom(n * k, 1, relevanceProb), nrow=n, ncol=k, byrow=F)
  weightMat <- relevant * matrix(rbeta(n * k, aPrior, bPrior), nrow=n, ncol=k, byrow=F)
  weightMat <- weightMat + (1 - relevant) * NRWeight #replace zeroes with tiny weight
  return(weightMat)
}

###############
#Model 2 - sample uniformly over 1 : n for each arm. This value is "relevant" and is drawn from a 
#Beta(aPrior, bPrior) distribution. All other values are 0.
###############
generateWeights2 <- function(k, n, aPrior, bPrior, relevanceProb, bPriorOther){
  weightMat <- matrix(NRWeight, nrow=n, ncol=k, byrow=F)
  return(addRelevantWeights(k, n, aPrior, bPrior, relevanceProb, weightMat, rep(1, k)))
}

###############
#Model 3 - as for model 2 but non-relevant are now drawn from a different bPrior distribution with
#parameters aPrior=1 and bPrior="bPriorOther".
###############
generateWeights3 <- function(k, n, aPrior, bPrior, relevanceProb, bPriorOther){
  weightMat <- matrix(rbeta(n * k, 1, bPriorOther), nrow=n, ncol=k, byrow=F)
  return(addRelevantWeights(k, n, aPrior, bPrior, relevanceProb, weightMat, rep(1, k)))
}

################
#Model 4 - uses a mixture distribution. The number of "relevant" values is given by 
# min(X, n), where X is a shifted geometric distribution with parameter "relevanceProb".
#Each "relevant" value is given a weight from a Beta(aPrior, bPrior) distribution 
# and zero if not relevant.
#Mean number of relevant values is 1 / relevantProb.
################
generateWeights4 <- function(k, n, aPrior, bPrior, relevanceProb, bPriorOther){
  numRelevantVec <- pmin(rgeom(k, relevanceProb) + 1, n)
  weightMat <- matrix(NRWeight, nrow=n, ncol=k, byrow=F)
  return(addRelevantWeights(k, n, aPrior, bPrior, relevanceProb, weightMat, numRelevantVec))
}

################
#Model 5 - similar to Model 4 except the number of relevant values for each arm comes from
# a Possion (1 / relevance + 1) shifted 1 to the right. This is done so that a value of 
# relevanceProb gives the same mean number of relevant values for models 4 & 5.
################
generateWeights5 <- function(k, n, aPrior, bPrior, relevanceProb, bPriorOther){
  numRelevantVec <- pmin(rpois(k, 1 / relevanceProb - 1) + 1, n)
  weightMat <- matrix(NRWeight, nrow=n, ncol=k, byrow=F)
  return(addRelevantWeights(k, n, aPrior, bPrior, relevanceProb, weightMat, numRelevantVec))
}

############
#Generates a dirichlet distribution.
#"a" is a vector of length n.
############
rdirichlet <- function(a) {
  y <- rgamma(length(a), a, 1)
  return(y / sum(y))
}

################
#Get values of q for a single run. Returned matrix has dim n x N.
#Each entry is "relevant" with probability relevanceProb. If non-relevant it is
# zero. Other entries are given a generalised discrete distribution.
#If all entries in a column are non-relevant then a single entry is made relevant.
#
# If FIXq==T then q is fixed to be the same for each time and each run. Its value
# is given by global parameter qCONSTANT.
#
# If DIRq==T then each q is an independently generated dirichlet distribution with parameters
# given by DIRICHLET_PARAMETERS in a randomised order.
################
generateq <- function(n, N, qSeed, qRelevanceProb=1){
  if (FIXq==T){
    q <- matrix(rep(qCONSTANT, N), nrow=n, ncol=N, byrow=F)
    return(q)
  }
  set.seed(qSeed)
  q <- matrix(runif(n * N), nrow=n, ncol=N, byrow=T)
  if (DIRq==T){
    for (i in 1 : N){
      q[, i] <- rdirichlet(sample(DIRICHLET_PARAMETERS))
    }
    return(q)
  }
  relevant <- matrix(rbinom(n * N, 1, qRelevanceProb), nrow=n, ncol=N, byrow=F)
  q <- q * relevant
  for (i in 1 : N){
    if (sum(q[, i])==0){q[sample.int(n, 1), i] <- 1} #ensure each column has non-zero entry
    q[, i] <- q[, i] / sum(q[, i]) #normalise
  }
  return(q)
}

##############
#Get values of x given q for a single run. A vector of length N is returned.
##############
generatex <- function(n, N, xSeed, qMat){
  set.seed(xSeed)
  x <- numeric(N)
  for (i in 1 : N){
    x[i] <- sample(1 : n, 1, prob=qMat[, i])
  }
  return(x)
}

################################
#Generates a matrix of uniform random numbers.
################################
generateRand <- function(N, k, seed){
  set.seed(seed)
  rand <- matrix(runif(k * N), byrow=T, ncol=N)
  return(rand)
}

###############
#Simulates a single run using the given policy.
#Policy names are of the form policynameXXX where XXX is either PCM or TCM and gives the 
# click model used by the policy which may be different to the true click model.
#If "saveDetail"=T then all outcomes, actions and final alpha and beta matrices are 
# returned as a list, otherwise just outcomes are returned. 
# Outcomes and actions are N x m matrices. a and b are n x k matrices.
#"updatingMethod" determines the method uses to update beliefs. These are described in
# the relevant functions. If cascade=T then only outcomes up to first success are 
# observed (all  observed if no success or cascade=F).
# a,b are n x k matrices with n values of each for each arm.
###############
simPolicy <- function(policy, k, n, N, m, a, b, wMat, qMat, xVec, randMat, uVec,
                      updatingMethod, saveDetail=F, tune=0.9, cascade=T, clickModel="PCM"){
  nPol <- nchar(policy)
  policyClickModel <- substring(policy, nPol - 2)
  policyRoot <- substring(policy, 1, nPol - 3)
  outcomes <- matrix(nrow=N, ncol=m)
  if(saveDetail){acts <- matrix(nrow=N, ncol=m)}
  funName <- paste("act", policyRoot, sep="")
  qFunName <- paste0("getPostq", postqType)
  for(i in 1 : N){
    act <- eval(call(funName, a, b, k, m, qMat[, i], tune=tune, wMat=wMat, clickModel=policyClickModel)) #vector of length m
    y <- getOutcome(m, xVec[i], act, wMat, randMat[, i], uVec[i], clickModel)
    outcomes[i, ] <- y
    if(saveDetail){acts[i, ] <- act}
    postq <- eval(call(qFunName, m, act, y, qMat[, i], wMat, a, b, cascade))
    a <- eval(call(paste0("updateAlpha", updatingMethod), m, a, b, act, y, postq, n, cascade))
    b <- eval(call(paste0("updateBeta", updatingMethod), m, a, b, act, y, postq, n, cascade))
  }
  if(saveDetail){return(list(outcomes * 1, acts, a, b))}
  return(outcomes)
}

###########
#Given a vector of chosen arms given by "act" this returns a binary vector
# of length m which correspond to success/failures from each arm in "act".
#The click model used is given by "clickModel" (either PCM or TCM).
###########
getOutcome <- function(m, x, act, wMat, randVec, u, clickModel="PCM"){
  if (clickModel=="PCM"){
    return(1 * (randVec[act] < wMat[x, act]))
  }else{
    return(1 * (u < wMat[x, act]))
  }
}

###########
#After y is observed we can update our belief in q. This returns the updated q.
#Uses known w. For PCM.
###########
getPostqPCMKnown <- function(m, act, y, qVec, wMat, a, b, cascade){
  if (cascade){
    if (sum(y) > 0){m <- min(which(y==1))}
  }
  postq <- qVec
  for (i in 1 : m){
    #The complicated equation simplifies depending on y=1 or y=0
    postq <- postq * (1 - wMat[, act[i]] + 2 * wMat[, act[i]] * y[i] - y[i])
  }
  postq <- postq / sum(postq)
  return(postq)
}

###########
#After y is observed we can update our belief in q. This returns the updated q.
#Uses known w. For TCM.
###########
getPostqTCMKnown <- function(m, act, y, qVec, wMat, a, b, cascade){
  if (cascade){
    if (sum(y) > 0){m <- min(which(y==1))}
  }
  postq <- qVec
  if (sum(y)==0){
    for(i in 1 : n){
      postq[i] <- postq[i] * (1 - max(wMat[i, act])) #apply is slower
    }
    return(postq / sum(postq))
  }
  if (m==1){
    postq <- postq * wMat[, act[1]]
  }else{
    for (i in 1 : n){
      gap <- max(0, wMat[i, act[m]] - max(wMat[i, act[1 : (m - 1)]]))
      postq[i] <- postq[i] * gap
    }
  }
  if (sum(postq)==0){return(qVec)} # return prior q (needed when called from getPostqTCMUnknown)
  postq <- postq / sum(postq)
  return(postq)
}

###########
#After y is observed we can update our belief in q. This returns the updated q.
#Uses posterior belief in w.
###########
getPostqPCMUnknown <- function(m, act, y, qVec, wMat, a, b, cascade){
  if (cascade){
    if (sum(y) > 0){m <- min(which(y==1))}
  }
  postq <- qVec
  mu <- a / (a + b)
  for (i in 1 : m){
    #The equation simplifies depending on y=1 or y=0
    postq <- postq * (1 - mu[, act[i]] + 2 * mu[, act[i]] * y[i] - y[i])
  }
  postq <- postq / sum(postq)
  return(postq)
}

###########
#After y is observed we can update our belief in q. This returns the updated q.
#Uses posterior belief in w. For TCM.
#This just uses mu as a substitute for the unknown w. Doesn't appear to work well.
###########
getPostqTCMUnknown <- function(m, act, y, qVec, wMat, a, b, cascade){
  mu <- a / (a + b)
  return(getPostqTCMKnown(m, act, y, qVec, mu, a, b, cascade))
}

###########
#After y is observed we can update our belief in q. This returns the updated q.
#Uses posterior belief in w. For TCM.
#Correct maths but uses Monte Carlo integration.
#ALTERNATIVE METHOD.
###########
getPostqTCMUnknownMC <- function(m, act, y, qVec, wMat, a, b, cascade){
  if (cascade){
    if (sum(y) > 0){m <- min(which(y==1))}
  }
  postq <- qVec
  if (sum(y)==0){
    for(i in 1 : n){
      wSamp <- matrix(rbeta(SAMPSIZE * m, a[i, act], b[i, act]), nrow=SAMPSIZE, ncol=m, byrow=T)
      wSampMaxes <- apply(wSamp, 1, max)
      postq[i] <- postq[i] * (1 - mean(wSampMaxes))
    }
    return(postq / sum(postq))
  }
  if (m==1){
    postq <- postq * mu[, act[1]]
  }else{
    for (i in 1 : n){
      wSamp <- matrix(rbeta(SAMPSIZE * m, a[i, act], b[i, act]), nrow=SAMPSIZE, ncol=m, byrow=T)
      wSampMaxes <- apply(wSamp[, -m, drop=F], 1, max)
      gap <- max(0, mean(wSamp[, m]) - mean(wSampMaxes))
      postq[i] <- postq[i] * gap
    }
  }
  #Two alternatives are given for dealing with postq all zero. Resampling better theoretically
  # but slower.
  if (sum(postq)==0){
    return(qVec)
    #postq <- getPostqTCMUnknown(m, act, y, qVec, wMat, a, b, cascade)
  }
  postq <- postq / sum(postq)
  return(postq)
}

###########
#Updates the alpha matrix by adding a proportion of a success equal to the posterior
# probability of x (given by "qVec").
#If cascade=T then only outcomes up to first success are observed (all  observed if no success
# or cascade=F).
###########
updateAlphaPCMDeter <- function(m, a, b, act, y, qVec, n, cascade, ...){
  if (cascade){
    if (sum(y) > 0){m <- min(which(y==1))}
  }
  for (i in 1 : m){
    a[, act[i]] <- a[, act[i]] + qVec * y[i]
  }
  return(a)
}

###########
#Updates the beta matrix by adding a proportion of a failure equal to the posterior
# probability of x (given by "qVec").
#If cascade=T then only outcomes up to first success are observed (all  observed if no success
# or cascade=F).
###########
updateBetaPCMDeter <- function(m, a, b, act, y, qVec, n, cascade, ...){
  if (cascade){
    if (sum(y) > 0){m <- min(which(y==1))}
  }
  for (i in 1 : m){
    b[, act[i]] <- b[, act[i]] + qVec * (1 - y[i])
  }
  return(b)
}

###########
#This just redirects to updateAlphaPCMDeter() as the method is the same for alpha updates.
###########
updateAlphaTCMDeter <- function(m, a, b, act, y, qVec, n, cascade, ...){
  return(updateAlphaPCMDeter(m, a, b, act, y, qVec, n, cascade))
}

###########
#Updates the beta matrix by adding a proportion of a failure equal to the posterior
# probability of x (given by "qVec").
#If cascade=T then only outcomes up to first success are observed (all  observed if no success
# or cascade=F).
#TCM version: a weight is sampled from the posterior for each w (wSamp). Weight w_i,x is only 
#updated if wSamp_i,x < wSamp_j,x for all j<i.
###########
updateBetaTCMDeter <- function(m, a, b, act, y, qVec, n, cascade, ...){
  if (cascade){
    if (sum(y) > 0){m <- min(which(y==1))}
  }
  wSamp <- matrix(rbeta(n * k, a, b), nrow=n, ncol=k)
  b[, act[1]] <- b[, act[1]] + qVec * (1 - y[1])  
  if (m > 1){
    for (i in 2 : m){
      for (x in 1 : n){
        if (max(wSamp[x, act[1 : (i - 1)]]) < wSamp[x, act[i]]){
          b[x, act[i]] <- b[x, act[i]] + qVec[x] * (1 - y[i])
        }
      }
    }
  }
  return(b)
}

###########
#Samples an x from postq then uses normal beta-bernoulli updating conditional on
# this value of x.
#If cascade=T then only outcomes up to first success are observed (all  observed if no success
# or cascade=F).
###########
updateAlphaPCMStoc <- function(m, a, b, act, y, qVec, n, cascade, ...){
  if (cascade){
    if (sum(y) > 0){m <- min(which(y==1))}
  }
  x <- sample(1 : n, m, prob=qVec, replace=T)
  for (i in 1 : m){
    a[x[i], act[i]] <- a[x[i], act[i]] + y[i]
  }
  
  return(a)
}

###########
#Samples an x from postq then uses normal beta-bernoulli updating conditional on
# this value of x.
#If cascade=T then only outcomes up to first success are observed (all  observed if no success
# or cascade=F).
###########
updateBetaPCMStoc <- function(m, a, b, act, y, qVec, n, cascade, ...){
  if (cascade){
    if (sum(y) > 0){m <- min(which(y==1))}
  }
  x <- sample(1 : n, m, prob=qVec, replace=T)
  for (i in 1 : m){
    b[x[i], act[i]] <- b[x[i], act[i]] + (1 - y[i])
  }
  return(b)
}

###########
#Samples an x from postq then uses normal beta-bernoulli updating conditional on
# this value of x.
#If cascade=T then only outcomes up to first success are observed (all  observed if no success
# or cascade=F).
#TCM version: a weight is sampled from the posterior for each w (wSamp). Weight w_i,x is only 
#updated if wSamp_i,x < wSamp_j,x for all j<i.
###########
updateBetaTCMStoc <- function(m, a, b, act, y, qVec, n, cascade, ...){
  if (cascade){
    if (sum(y) > 0){m <- min(which(y==1))}
  }
  x <- sample(1 : n, 1, prob=qVec, replace=T)
  wSamp <- rbeta(k, a[x, ], b[x, ])
  b[x, act[1]] <- b[x, act[1]] + (1 - y[1])  
  if (m > 1){
    for (i in 2 : m){
      if (max(wSamp[act[1 : (i - 1)]]) < wSamp[act[i]]){
        b[x[i], act[i]] <- b[x[i], act[i]] + (1 - y[i])
      }
    }
  }
  return(b)
}
##########
#Returns chosen arms using a Naive Greedy policy which chooses arms in order of decreasing
# independent CTR.
##########
actNGreedy <- function(a, b, k, m, q, ...){
  mu <- a / (a + b)
  p <- as.vector(q %*% mu) #returns a k length vector
  return(order(p, decreasing=TRUE)[1 : m])
}

##########
#Returns chosen arms using the Naive Thompson Sampling policy. Arms are chosen 
#in order of decreasing Thompson sampled value.
###############
actNThompson <- function(a, b, k, m, q, ...){
  scoreTS <- getTS(a, b, k)
  p <- as.vector(q %*% scoreTS) #returns a k length vector
  return(order(p, decreasing=TRUE)[1 : m])
}

##########
#Returns chosen arms using the Naive Interval Estimation policy. Arms are chosenin order
# of poseterior quantile value given by the argument "tune".
###############
actNIE <- function(a, b, k, m, q, tune, clickModel, ...){
  n <- dim(a)[1]
  score <- matrix(nrow=n, ncol=k)
  for (i in 1 : k){
    score[, i] <- qbeta(tune, a[, i], b[, i])
  }
  p <- as.vector(q %*% score)
  return(order(p, decreasing=TRUE)[1 : m])
}

###############
#This chooses arms sequentially such that each arm chosen maximises the
#improvement in CTR given the existing arms **if** the supplied "score"
#argument are the correct weights. "score" is determined by individual policies (n x k matrix)
#e.g. greedy uses the posterior mean, TS samples from the posterior etc.
#The "clickModel" argument sets the click model used (PCM or TCM).
###############
getGreedySet <- function(score, k, m, q, clickModel="PCM"){
  acts <- numeric(m)
  p <- as.vector(q %*% score)
  if(m==1){return(which.max(p))}
  funcName <- paste0("getContribution", clickModel)
  acts[1] <- which.max(p)
  for (i in 2 : m){
    contribution <- eval(call(funcName, acts, score, q, k))
    contribution[acts] <- -1 #ensures current ads are not selected again
    options <- contribution==max(contribution) #arms that tie for max contribution
    acts[i] <- which.max(p * options) #break ties using score
  }
  return(acts)
}

#################
#For PCM. Returns a vector of contribution or marginal gain of the each arm if it was added to the
# existing set of actions given by "acts". The argument "score" is an n x k matrix of scores 
# assigned to each arm weight (depends on policy).
#################
getContributionPCM <- function(acts, score, q, k){
  possContrib <- apply(1 - score[, acts, drop=F], 1, prod)
  return(colSums(q * score * possContrib))
}

#################
#For TCM. Returns a vector of contribution or marginal gain of the each arm if it was added to the
# existing set of actions given by "acts". The argument "score" is an n x k matrix of scores 
# assigned to each arm weight (depends on policy).
#################
getContributionTCM <- function(acts, score, q, k){
  contribution <- numeric(k)
  pmaxExisting <- apply(score[, acts, drop=F], 1, max)
  for (j in 1 : k){
    contribution[j] <- sum(q * pmax(score[, j], pmaxExisting))
  }
  return(contribution)
}

#############
#Returns actions chosen by enumerating all sets assuming true weights are known.
#ARM_SETS is a global variable which is combn(k, m). It is done this way to save speed 
# (same for all runs).
#############
actOptimal <- function(a, b, k, m, q, wMat, clickModel, ...){
  ctrMat <- apply(ARM_SETS, 2, getExpectCTR, q, wMat, clickModel)
  return(ARM_SETS[, which.max(ctrMat)])
}

#############
#Returns actions chosen by a greedy policy assuming true weights are known.
#############
actSOracle <- function(a, b, k, m, q, wMat, clickModel, ...){
  return(getGreedySet(wMat, k, m, q, clickModel))
}

##########
#Returns chosen arms using a Greedy policy which chooses arms sequentially such that
#each arm chosen maximises the improvement in CTR given the existing arms. 
##########
actSGreedy <- function(a, b, k, m, q, clickModel, ...){
  score <- a / (a + b)
  return(getGreedySet(score, k, m, q, clickModel))
}

##########
#Returns chosen arms using a Conditional Greedy policy. This samples x for each action
#then chooses the arm with the highest mu given the sampled x.
##########
actCGreedy <- function(a, b, k, m, q, ...){
  mu <- a / (a + b)
  n <- dim(a)[1]
  x <- sample.int(n, m, replace=T)
  acts <- numeric(m)
  acts[1] <- which.max(mu[x[1], ])
  p <- numeric(k)
  for (i in 2 : m){
    p <- mu[x[i], ]
    p[acts] <- -1
    acts[i] <- which.max(p)
  }
  return(acts)
}

###############
#Returns a sample from the weight posteriors, one for each weight as a n x k matrix. 
###############
getTS <- function(a, b, k){
  n <- dim(a)[1]
  samp <- matrix(nrow=n, ncol=k)
  for (i in 1 : k){
    samp[, i] <- rbeta(n, a[, i], b[, i])
  }
  return(samp)
}

##########
#Returns chosen arms using the Thompson Sampling policy. Arms are chosen sequentially
# as for the Greedy policy but, for each arm weight, instead of using mu it uses a 
#value sampled from the weight belief.
###############
actSThompson <- function(a, b, k, m, q, clickModel, ...){
  score <- getTS(a, b, k)
  return(getGreedySet(score, k, m, q, clickModel))
}

############################
#Simulates multiple runs using the given policy. The number of runs is given by "runs".
#Each run generates the scenario and random numbers for that run base on the supplied seeds
# and calls simPolicy().
#If saveDetail=T a list of lists is returned where each sublist is the result of a single run 
# (see simPolicy for details).
# if saveDetail=F then a matrix is returned (runs x N). Row i records the rewards at each 
# time in run i.
############################
runSims <- function(policy, k, n, N, m, aPrior, bPrior, runs, 
                    wSeedVec, qSeedVec, xSeedVec, randSeedVec, uSeedVec,
                    wModel=1, relevanceProb=0.6, bPriorOther=40, qRelevanceProb=1,
                    updatingMethod, saveDetail=F, tune=0.9, cascade=T, clickModel="PCM"){
  a <- matrix(aPrior, nrow=n, ncol=k)
  b <- matrix(bPrior, nrow=n, ncol=k)
  rewards <- matrix(nrow=runs, ncol=N) #reward at each time.
  if(saveDetail){results <- vector('list', runs)}
  for(i in 1 : runs){
    #initialise run
    wMat <- generateWeightsMaster(k, n, aPrior, bPrior, wSeedVec[i], wModel, relevanceProb, bPriorOther)
    qMat <- generateq(n, N, qSeedVec[i], qRelevanceProb)
    xVec <- generatex(n, N, xSeedVec[i], qMat)
    set.seed(uSeedVec[i])
    uVec <- runif(N)
    randMat <- generateRand(N, k, randSeedVec[i])
    #run sim and record results
    runResult <- simPolicy(policy, k, n, N, m, a, b, wMat, qMat, xVec, randMat, uVec,
                           updatingMethod, saveDetail, tune, cascade, clickModel)
    if(saveDetail){
      results[[i]] <- runResult
    }else{
      rewards[i, ] <- apply(runResult, 1, max) #converts outcomes to rewards
    }
  } 
  if(saveDetail){return(results)}
  return(rewards)
}

###########
#Given a matrix of rewards (runs x N) this returns the means over the runs (length N vector).
###########
processRewards <- function(rewards){
  return(colMeans(rewards))
}

###########
#Returns a length N vector of mean rewards at each time averaged over all runs.
#This works for output from runGreedyOracleSims() as well as runSims() and with 
# any saveDetail setting (provided it is given as an argument).
###########
getMeanRewards <- function(results, saveDetail=F){
  rewards <- getRewards(results, saveDetail)
  return(colMeans(rewards))
}

#############
#Returns the mean regret of a list of mean Rewards over all policies averaged over all runs.
#Regret is relative to the "main" policy.
#Returns a list of vectors each of length N. The list does not have an entry for the "main" policy
#############
getRegretFromMeanRewards <- function(meanRewards, pols, main="SOracle"){
  l <- length(pols)
  mainIndex <- which(pols==main)
  if (length(mainIndex)==0){
    stop("Comparison policy not found")
  }
  cat("Comparison policy is", main, "\n")
  regret <- vector('list', l)
  names(regret) <- pols
  for (i in 1 : l){
    regret[[i]] <- meanRewards[[mainIndex]] - meanRewards[[i]]
  }
  regret[mainIndex] <- NULL
  return(regret)
}

###############
#Extracts mean rewards for all policies over all runs for a single experiment.
#The mean rewards are stored in a list of vectors (one vector of length N for each policy).
###############
getMeanRewardsPols <- function(resultsPols, pols, saveDetail){
  l <- length(pols)
  meanRewards <- vector('list', l)
  names(meanRewards) <- pols
  for (i in 1 : l){
    time <- system.time(meanRewards[[i]] <- getMeanRewards(resultsPols[[i]], saveDetail))
    if (i==1) {cat("Processing results from", l, "policies ", sep=" ")}
    cat(i)
  }
  cat("\n")
  return(meanRewards)
}

#############
#Extracts the reward matrix (runs x N) for a single policy given that policy's results.
#If saveDetail=F then the output equals the input.
#############
getRewards <- function(results, saveDetail=F){
  if (saveDetail){
    N <- dim(results[[1]][[1]])[1]
    runs <- length(results)
    rewards <- matrix(nrow=runs, ncol=N)
    for (i in 1 : runs){
      rewards[i, ] <- apply(results[[i]][[1]], 1, max)
    }
  }else{
    rewards <- results
  }
  return(rewards)
}

###########
#Processes the output from runSims() when saveDetail=T.
#At the moment this is a bit pointless but I will keep as a possible skeleton for later.
###########
processResults <- function(results){
  N <- dim(results[[1]][[1]])[1]
  m <- dim(results[[1]][[1]])[2]
  n <- dim(results[[1]][[3]])[1]
  k <- dim(results[[1]][[3]])[2]
  runs <- length(results)
  rewards <- matrix(nrow=runs, ncol=N)
  actCounts <- matrix(nrow=runs, ncol=k)
  aSum <- matrix(0, nrow=n, ncol=k)
  bSum <- matrix(0, nrow=n, ncol=k)
  actVariances <- numeric(runs)
  for (i in 1 : runs){
    rewards[i, ] <- apply(results[[i]][[1]], 1, max)
    actCounts[i, ] <- as.numeric(table(results[[i]][[2]]))
    actVariances[i] <- var(sort(actCounts[i, ]) / sum(actCounts[i, ]))
    aSum <- aSum + results[[i]][[3]]
    bSum <- bSum + results[[i]][[4]]
  }
  meanRewards <- processRewards(rewards)
  meanActs <- colMeans(actCounts)
  aMean <- aSum / runs
  bMean <- bSum / runs
  return(list(meanRewards, meanActs, actVariances, aMean, bMean))
}

#############
#Calls runSims() over a range of policies given by "pols". Return a list of results.
#Also prints the run time of each policy to the screen.
#############
simMultiplePolicies <- function(pols, runs, k, n, N, m, aPrior=1, bPrior=1, wModel=1,
                                relevanceProb=1, bPriorOther=40, qRelevanceProb=1,
                                wSeedVec=(1 : (runs + 1)), qSeedVec=(100001 : (runs + 100001)),
                                xSeedVec=(200001 : (runs + 200001)), randSeedVec=(300001 : (runs + 300001)),
                                uSeedVec=400001 : (runs + 400001), saveDetail=T, updatingMethod, 
                                tune=0.9, cascade=T, clickModel="PCM", ...){
  l <- length(pols)
  resultsPols <- vector('list', l)
  meanRewardsPols <- vector('list', l)
  time <- numeric(l)
  for (i in 1 : l){
    time[i] <- system.time(resultsPols[[i]] <- runSims(pols[i], k, n, N, m, aPrior, bPrior, runs, 
                                                       wSeedVec, qSeedVec, xSeedVec, randSeedVec, uSeedVec,
                                                       wModel, relevanceProb, bPriorOther, qRelevanceProb,
                                                       updatingMethod, saveDetail, tune, cascade, 
                                                       clickModel))[3]
    cat(pols[i], time[i], "\n")
  }
  return(resultsPols)
}

###########
#Returns the total regret (cumulative regret at time N) of the supplied policies (pols[-main]).
#If percent=T then the total regret is divided by the total regret of the "main" policy.
##########
getTotalRegret <- function(resultsPols, regret, pols, percent=F, main="SOracle"){
  l <- length(pols)
  mainIndex <- which(pols==main)
  if (length(mainIndex)==0){
    stop("Comparison policy not found")
  }
  meanOracleResults <- getMeanRewards(resultsPols[[mainIndex]], saveDetail)
  totalRegret <- round(sapply(regret, cumsum)[N, ], 4)
  if (percent){totalRegret <- totalRegret / sum(meanOracleResults)}
  names(totalRegret) <- pols[-which(pols==main)]
  return(totalRegret)
}

#############
#Plots cumulative regret over time."regret" is a list of vectors with one vector of mean regret
#values over time for each policy
#If "percent=T" then regret is given as a percentage of end reward of the "main" policy. 
#Set yUpperLim="auto" to fit the data or a number otherwise for the maximum y axis value. 
#############
plotRegret <- function(regret, meanRewards, pols, percent=F, plotTitle, mainPol="SOracle", polNames,
                       plot_col=1:24, plot_lwd=1, legend_cex=1, yUpperLim="auto", setMargin=T){
  if (is.null(plotTitle) & setMargin){par(mar=c(4.5, 4, 0.2, 0.9)+0.1)}
  l <- length(regret)
  mainIndex <- which(pols==mainPol)
  if (!exists("polNames")){polNames <- pols[-mainIndex]}
  cumRegret <- vector('list', l)
  for (i in 1 : l){
    cumRegret[[i]] <- cumsum(regret[[i]])
    if (percent){cumRegret[[i]] <- 100 * cumRegret[[i]] / sum(meanRewards[[mainIndex]])}
  }
  if (yUpperLim=="auto"){
    yu <- max(unlist(cumRegret))
  }else{
    yu <- yUpperLim
  }
  yl <- min(unlist(cumRegret))
  plot(cumRegret[[1]], ylim=c(yl, yu), typ='l', ylab=paste('Cumulative Regret',if(percent){"%"}), xlab='Time',
       main=plotTitle, col=plot_col[1], lwd=plot_lwd)
  if (l > 1){
    for (i in 2 : l){
      lines(cumRegret[[i]], col=plot_col[i], lty=i, lwd=plot_lwd)
    }
  }
  legend("topleft", polNames, col=plot_col[1 : l], lty=1 : l, cex=legend_cex, lwd=plot_lwd)
}

#############
#Returns the expected CTR over q for a set of arms "acts" given q and wMat.
#############
getExpectCTR <- function(acts, q, wMat, clickModel){
  if (clickModel=="PCM"){
    return(sum(q * (1 - apply(1 - wMat[, acts], 1, prod))))
  }else{
    return(sum(q * apply(wMat[, acts], 1, max)))
  }
}

###############
#Returns a vector of the difference between the true best weight for each x and the true weight of arm
# with the highest mu.
###############
getPostErrorBest <- function(wMat, aMat, bMat){
  mu <- aMat / (aMat + bMat)
  trueBest <- apply(wMat, 1, max)
  bestArms <- apply(mu, 1, which.max)
  muError <- trueBest - wMat[cbind(1 : n, bestArms)]
  return(muError)
}

###############
#Returns the difference between the true best weight for each x and the true weight of arm
# with the highest mu (averaged over all runs).
###############
getMeanPostErrorBest <- function(wMatList, results){
  runs <- length(results)
  n <- dim(wMatList[[1]])[1]
  errorMat <- matrix(nrow=n, ncol=runs)
  for (i in 1 : runs){
    wMat <- wMatList[[i]]
    aMat <- results[[i]][[3]]
    bMat <- results[[i]][[4]]
    errorMat[, i] <- getPostErrorBest(wMat, aMat, bMat)
  }
  meanError <- rowSums(errorMat) / runs
  return(mean(meanError))
}

###############
#For all policies returns the difference between the true best weight for each x and the true weight of arm
# with the highest mu (averaged over all runs).
###############
getMeanPostErrorBestPols <- function(pols, resultsPols, k, n, aPrior, bPrior, wSeedVec, wModel, relevanceProb, bPriorOther){
  l <- length(pols)
  meanErrors <- numeric(l)
  wMatList <- getwMatList(k, n, aPrior, bPrior, wSeedVec, wModel, relevanceProb, bPriorOther)
  for (i in 1 : l){
    meanErrors[i] <- getMeanPostErrorBest(wMatList, resultsPols[[i]])
  }
  names(meanErrors) <- pols
  return(meanErrors)
}

##############
#Returns a list of weight matrices created using the supplied settings. The seeds for each are given by wSeedVec.
##############
getwMatList <- function(k, n, aPrior, bPrior, wSeedVec, wModel, relevanceProb, bPriorOther){
  runs <- length(wSeedVec)
  wMatList <- vector('list', runs)
  for (i in 1 : runs){
    wMatList[[i]] <- generateWeightsMaster(k, n, aPrior, bPrior, wSeedVec[i], wModel, relevanceProb, bPriorOther)
  }
  return(wMatList)
}

#==================================
#Functions for viewing updating over time.
#Used in update_detail_DO.R
#==================================

###############
#Simulates a single run as with SimPolicy() but returns different information.
#Used to test w estimate convergence.
#Returns a list of outcomes, aList, bList where aList and bList are list containing, 
#respectively, the a, b matrices at each time.
#Intended to be used directly (single run rather than from runSims).
#Assumes k=m=1 so no decisions made.
###############
simUpdateDetail <- function(n, N, a, b, wMat, qMat, xVec, randMat, uVec,
                            updatingMethod, cascade=T, clickModel="PCM"){
  outcomes <- numeric(N)
  qFunName <- paste0("getPostq", postqType)
  aList <- vector('list', N)
  bList <- vector('list', N)
  act <- 1
  for(i in 1 : N){
    y <- getOutcome(1, xVec[i], act, wMat, randMat[, i], uVec[i], clickModel)
    outcomes[i] <- y
    postq <- eval(call(qFunName, m, act, y, qMat[, i], wMat, a, b, cascade))
    a <- eval(call(paste0("updateAlpha", updatingMethod), m, a, b, act, y, postq, n, cascade))
    b <- eval(call(paste0("updateBeta", updatingMethod), m, a, b, act, y, postq, n, cascade))
    aList[[i]] <- a
    bList[[i]] <- b
  }
  return(list(outcomes, aList, bList))
}

##############
#Returns matrix of w estimates over time (N x n). Inputs aList, bList
# are lists of length N of matrices of alpha, beta values.
##############
getwHat <- function(n, N, aList, bList){
  aMat <- matrix(nrow=N, ncol=n)
  bMat  <- matrix(nrow=N, ncol=n)
  for (i in 1 : N){
    aMat[i, ] <- aList[[i]]
    bMat[i, ] <- bList[[i]]
  }
  return(aMat / (aMat + bMat))
}

##############
#Plots w estimates over time against true w.
##############
plotwHat <- function(n, wHatMat, wMat){
  plot(wHatMat[, 1], ylim=c(0,1), type='l')
  for(i in 2 : n){
    lines(wHatMat[, i], col=i)
  }
  for (i in 1 : n){
    abline(h=wMat[i, 1], lty=3, col=i)
  }
}

#==================================
#Functions to rename policies for paper.
#==================================

##############
#Renames a vector of policy names using renamePolicy().
##############
renamePols <- function(pols){
  newPols <- c()
  for (i in 1 : length(pols)){
    newPols[i] <- renamePolicy(pols[i])
  }
  return(newPols)
}

##############
#Renames the supplied policy to be appropriate for use in a plot legend.
#"policy" will be in form "SThompsonTCM" - S or N for set choosing, then bandit
# policy name, the click model used.
#The otput is in form "STS-TCM".
##############
renamePolicy <- function(policy){
  nPol <- nchar(policy)
  policyClickModel <- substring(policy, nPol - 2)
  explore <- substring(policy, 2, nPol - 3)
  setChoose <- substring(policy, 1, 1)
  fullNames <- c("Thompson", "Greedy")
  shortNames <- c("TS", "G")
  if (any(fullNames==explore)){
    newExplore <- shortNames[which(fullNames==explore)]
  }else{
    newExplore <- explore
  }
  newName <- paste0(setChoose, newExplore)
  if (setChoose!="N"){newName <- paste0(newName, "-", policyClickModel)}
  return(newName)
}
