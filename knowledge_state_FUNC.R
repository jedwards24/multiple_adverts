###############
# As standard simPolicy() but saves only posterior beliefs (as matrices of alpha and beta values).
# The frequency over time with which these are saved is given by the global variable "POST_SAVE_FREQ".
###############
simPolicyKS <- function(policy, k, n, N, m, a, b, wMat, qMat, xVec, randMat, uVec,
                        updatingMethod, saveDetail=F, tune=0.9, cascade=T, clickModel="PCM"){
  nPol <- nchar(policy)
  policyClickModel <- substring(policy, nPol - 2)
  policyRoot <- substring(policy, 1, nPol - 3)
  aList <- vector('list', N%/%POST_SAVE_FREQ)
  bList <- vector('list', N%/%POST_SAVE_FREQ)
  funName <- paste("act", policyRoot, sep="")
  qFunName <- paste0("getPostq", postqType)
  for(i in 1 : N){
    act <- eval(call(funName, a, b, k, m, qMat[, i], tune=tune, 
                     wMat=wMat, clickModel=policyClickModel))
    y <- getOutcome(m, xVec[i], act, wMat, randMat[, i], uVec[i], clickModel)
    postq <- eval(call(qFunName, m, act, y, qMat[, i], wMat, a, b, cascade))
    a <- eval(call(paste0("updateAlpha", updatingMethod), m, a, b, act, y, postq, n, cascade))
    b <- eval(call(paste0("updateBeta", updatingMethod), m, a, b, act, y, postq, n, cascade))
    if ((i %% POST_SAVE_FREQ)==0){
      aList[[i%/%POST_SAVE_FREQ]] <- a
      bList[[i%/%POST_SAVE_FREQ]] <- b
    }
  }
  return(list(aList, bList))
}

############################
# As the standard runSims() function except a list of posterior beliefs is returned as given in
# simPolicyKS() above.
############################
runSimsKS <- function(policy, k, n, N, m, aPrior, bPrior, runs, 
                      wSeedVec, qSeedVec, xSeedVec, randSeedVec, uSeedVec,
                      wModel=1, relevanceProb=0.6, bPriorOther=40, qRelevanceProb=1,
                      updatingMethod, saveDetail=F, tune=0.9, cascade=T, clickModel="PCM"){
  a <- matrix(aPrior, nrow=n, ncol=k)
  b <- matrix(bPrior, nrow=n, ncol=k)
  rewards <- matrix(nrow=runs, ncol=N) #reward at each time.
  posteriorsListRuns <- vector('list', runs)
  for(i in 1 : runs){
    #initialise run
    wMat <- generateWeightsMaster(k, n, aPrior, bPrior, wSeedVec[i], wModel, relevanceProb, bPriorOther)
    qMat <- generateq(n, N, qSeedVec[i], qRelevanceProb)
    xVec <- generatex(n, N, xSeedVec[i], qMat)
    set.seed(uSeedVec[i])
    uVec <- runif(N)
    randMat <- generateRand(N, k, randSeedVec[i])
    #run sim and record results
    posteriorsList <- simPolicyKS(policy, k, n, N, m, a, b, wMat, qMat, xVec, randMat, uVec,
                                  updatingMethod, saveDetail, tune, cascade, clickModel)
    posteriorsListRuns[[i]] <- posteriorsList
  }
  return(posteriorsListRuns)
}

###############
#These functions are used to get the posterior greedy regret given a belief state.
#A single action is taken using these and compared to the oracle.
###############

###############
#Gets the posterior greedy regret given a list of belief states over runs and times ("posteriorListRuns").
#A single action is taken using these and compared to the reward using the SOracle.
#For each run at each time the number of single actions taken is given by "repetitions". The reward is averaged
# over these.
# A matrix of regrets is returned with a row for each run.
###############
getExpectRegretKS <- function(testPolicy, k, n, N, m, repetitions, qRelevanceProb, tune, cascade, 
                              clickModel, posteriorsListRuns, postqSeedVec, wSeedVec, timeIndexSeq){
  ltis <- length(timeIndexSeq)
  regretMat <- matrix(nrow=runs, ncol=ltis)
  for (i in 1 : runs){
    wSeed <- wSeedVec[i]
    postqSeed <- postqSeedVec[i] 
    posteriorsList <- posteriorsListRuns[[i]]
    wMat <- generateWeightsMaster(k, n, aPrior, bPrior, wSeed, wModel, relevanceProb, bPriorOther)
    postqMat <- generateq(n, repetitions, postqSeed, qRelevanceProb)
    oracleCTR <- getOracleCTRRunKS(wMat, postqMat, k, n, N, m, repetitions, tune, cascade, clickModel)
    regretMat[i, ] <- getRegretRunKS(testPolicy, k, n, N, m, repetitions, wMat, postqMat, qRelevanceProb,
                                     tune, cascade, clickModel, posteriorsList, oracleCTR, timeIndexSeq)
  }
  return(regretMat)
}

###############
#Gets the posterior greedy regret given a list of belief states a single run and several times ("posteriorList").
#A single action is taken using these and compared to the reward using the SOracle.
#At each time the number of single actions taken is given by "repetitions". The reward is averaged
# over these.
# A vector of regrets is returned corresponding to times.
###############
getRegretRunKS <- function(testPolicy, k, n, N, m, repetitions, wMat, postqMat, qRelevanceProb,
                           tune, cascade, clickModel, posteriorsList, oracleCTR, timeIndexSeq){
  ltis <- length(timeIndexSeq)
  CTRVec <- numeric(ltis)
  for (i in 1 : ltis){
    aMat <- posteriorsList[[1]][[timeIndexSeq[i]]]
    bMat <- posteriorsList[[2]][[timeIndexSeq[i]]]
    acts <- runSingleActionSims(testPolicy, k, n, N, m, repetitions, aMat, bMat, wMat, 
                                postqMat, tune, cascade, clickModel)
    CTRVec[i] <- mean(getExpectCTRReps(acts, wMat, postqMat, repetitions))
  }
  return(oracleCTR - CTRVec)
}

###############
#Gets the expected posterior greedy reward (CTR) using the true weight matrix supplied 
# and the policy given by COMPARE POLICY (either SOracle or Optimal).
#A single actions taken as given by "repetitions". The reward is averaged over these.
# A scalar is returned.
###############
getOracleCTRRunKS <- function(wMat, postqMat, k, n, N, m, repetitions, tune, cascade, clickModel){
  acts <- runSingleActionSims(COMPARE_POLICY, k, n, N, m, repetitions, 
                              aMat=NULL, bMat=NULL, wMat, postqMat, tune, cascade, clickModel)
  ctrs <- getExpectCTRReps(acts, wMat, postqMat, repetitions)
  return(mean(ctrs))
}

###############
#Returns a matrix of the difference between the wMat and mu from the supplied aMat and bMat.
###############
getPostErrors <- function(wMat, aMat, bMat){
  mu <- aMat / (aMat + bMat)
  return(abs(mu-wMat))
}

############################
#Returns the actions for multiple runs ("repetitions") where the actions are taken at a single time using "policy" and
# the supplied belief matrices a and b.
#The actions are returned in a repitions x m matrix with each row being the result of a repetition.
############################
runSingleActionSims <- function(policy, k, n, N, m, repetitions, aMat, bMat, 
                                wMat, postqMat, tune, cascade, clickModel){
  nPol <- nchar(policy)
  policyClickModel <- substring(policy, nPol - 2)
  policyRoot <- substring(policy, 1, nPol - 3)
  actions <- matrix(nrow=repetitions, ncol=m)
  fun.name <- paste("act", policyRoot, sep="")
  for(i in 1 : repetitions){
    actions[i, ] <- eval(call(fun.name, aMat, bMat, k, m, postqMat[, i], 
                              tune=tune, wMat=wMat, clickModel=policyClickModel))
  } 
  return(actions)
}

###############
#Returns expected CTRs over q for each repetition given a matrix of actions (runs x m).
#The weights wMat are the same for each repetition and q for repetition i is given by row i of qMat.
###############
getExpectCTRReps <- function(actions, wMat, postqMat, repetitions){
  CTRs <- numeric(repetitions)
  for (i in 1 : repetitions){
    CTRs[i] <- getExpectCTR(actions[i, ], postqMat[, i], wMat, clickModel)
  }
  return(CTRs)
}

