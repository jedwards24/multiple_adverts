# Multiple Adverts README

This code accompanies the paper "Selecting Multiple Web Adverts - a Contextual Multi-armed Bandit with
State Uncertainty" currently in submission to JORS. It is designed to recreate the simulation results produced for the paper. 

### Repository Overview ###

There are three main types of files:

* `..._FUNC.R` - contain functions.
* `..._PLOT.R` - code to produce plots.
* `..._DO.R` - these are the files to be run. They run simulations then source plot files to produce plots.

A number of settings and functions will be redudant for the purpose for reproducing results from the paper as the code has been used for exploring different settings and options as part of the research.

### Getting Started ###

There are four groups of simulations run in the paper:

* Updating analysis in Section 4.3 (figures 1 and 2). Produced using `updating_DO.R`. Change `FIXq` to `T` or `F` to get the two plots.
* Regret simulations in Section 6.1.1 (figures 3 and 4). Produced using `adverts_DO.R`. Change `k`, `m` and `n` as given in the text to give the two plots although PCM and TCM have to be run separately. 
* Regret simulations using a fixed scenario in Section 6.1.2 (figures 5 and 6). Produced using `scenario_DO.R`. This produces both plots although PCM and TCM have to be run separately.
* State of knowledge simulations in Section 6.2 (figures 7 and 8). Produced using `knowledge_state_DO.R`. This produces both plots although PCM and TCM have to be run separately.

In each case the full versions of the simulations run for the paper can take a long time to run so use on smaller versions first (the default settings in code are ok). 

### Author and Contact ###

* James Edwards <j.edwards4@lancaster.ac.uk>
* I have more extensive code with options not used in the paper. Contact me if you are interested in extending the research.